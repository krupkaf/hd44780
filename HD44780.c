#include "HD44780.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#ifdef HD44780_SCROLL
uint8_t _HD44780_x, _HD44780_y;
#endif

#if HD44780_LINES == 1
static uint8_t _HD44780_BASE_Y[1] = {0x80};
#elif HD44780_LINES == 2
static uint8_t _HD44780_BASE_Y[2] = {0x80, 0xC0};
#endif

#define HD44780_RW_Set                                                         \
  { HD44780_RW_PORT |= _BV(HD44780_RW_BIT); }
#define HD44780_RW_Clr                                                         \
  { HD44780_RW_PORT &= ~_BV(HD44780_RW_BIT); }

#define HD44780_EN_Set                                                         \
  { HD44780_EN_PORT |= _BV(HD44780_EN_BIT); }
#define HD44780_EN_Clr                                                         \
  { HD44780_EN_PORT &= ~_BV(HD44780_EN_BIT); }

#define HD44780_RS_Set                                                         \
  { HD44780_RS_PORT |= _BV(HD44780_RS_BIT); }
#define HD44780_RS_Clr                                                         \
  { HD44780_RS_PORT &= ~_BV(HD44780_RS_BIT); }

#define HD44780_DATA_DOUT                                                      \
  {                                                                            \
    register uint8_t tmp = SREG;                                               \
    cli();                                                                     \
    HD44780_DATA_DDR |= HD44780_DATA_MASK;                                     \
    SREG = tmp;                                                                \
  }
#define HD44780_DATA_DIN                                                       \
  {                                                                            \
    register uint8_t tmp = SREG;                                               \
    cli();                                                                     \
    HD44780_DATA_DDR &= ~HD44780_DATA_MASK;                                    \
    SREG = tmp;                                                                \
  }

void _HD44780LongDelay(void) { _delay_ms(100); }
void _HD44780Delay(void) { _delay_us(1); }

void _HD44780WriteNibble(uint8_t data) {
  register uint8_t tmp = SREG;
  cli();
  HD44780_DATA_PORT =
      (HD44780_DATA_PORT & ~HD44780_DATA_MASK) | (data & HD44780_DATA_MASK);
  SREG = tmp;
  HD44780_EN_Set;
  _HD44780Delay();
  HD44780_EN_Clr;
  _HD44780Delay();
}

uint8_t _HD44780ReadNibble(void) {
  uint8_t data;
  HD44780_EN_Set;
  _HD44780Delay();
  data = HD44780_DATA_PIN & HD44780_DATA_MASK;
  HD44780_EN_Clr;
  _HD44780Delay();
  return data;
}

void _HD44780WriteData(uint8_t data) {
  HD44780_RW_Clr;
  HD44780_DATA_DOUT;
  _HD44780WriteNibble(data);
  asm volatile("swap %0" : "=r"(data) : "0"(data)); // data <<=4;
  _HD44780WriteNibble(data);
  HD44780_RW_Set;
}

void _HD44780Ready(void) {
  uint16_t i = 0;
  HD44780_DATA_DIN;
  HD44780_RW_Set;
  HD44780_RS_Clr;
  do {
    i++;
    _HD44780Delay();
    HD44780_EN_Set;
    _HD44780Delay();
    if (i > 5000) {
      break;
    }
  } while ((HD44780_DATA_PIN & HD44780_DATA_BUSY) != 0);
  HD44780_EN_Clr;
}

/* write a byte to the LCD character generator or display RAM */
void _HD44780WriteByte(uint8_t addr, uint8_t data) {
  _HD44780Ready();
  _HD44780WriteData(addr);
  _HD44780Ready();
  HD44780_RS_Set;
  _HD44780WriteData(data);
}

// read a byte from the LCD character generator or display RAM
uint8_t _HD44780ReadByte(uint8_t addr) {
  uint8_t data, x;
  _HD44780Ready();
  _HD44780WriteData(addr);
  _HD44780Ready();

  HD44780_DATA_DIN;
  HD44780_RS_Set;
  _HD44780Delay();
  data = _HD44780ReadNibble();
  x = _HD44780ReadNibble();
  asm volatile("swap %0" : "=r"(x) : "0"(x));
  data |= x;
  HD44780_RS_Clr;
  return (data);
}

void HD44780Init(void) {

  HD44780_EN_DDR |= _BV(HD44780_EN_BIT);
  HD44780_RS_DDR |= _BV(HD44780_RS_BIT);
  HD44780_RW_DDR |= _BV(HD44780_RW_BIT);

  HD44780_EN_Clr;
  HD44780_RS_Clr;

  _HD44780LongDelay();
  _HD44780WriteData(0x20);
  _HD44780LongDelay();
  _HD44780WriteData(0x20);
  _HD44780LongDelay();
  _HD44780WriteData(0x28);

  HD44780Cursor(0);
  HD44780Clear();
}

void HD44780Cursor(uint8_t c) {
  _HD44780Ready();
  if (c) {
    _HD44780WriteData(0x0e);
  } else {
    _HD44780WriteData(0x0c);
  };
}

void HD44780Clear(void) {
  _HD44780Ready();
  _HD44780WriteData(1);
#ifdef HD44780_SCROLL
  _HD44780_x = _HD44780_y = 0;
#endif
}

void HD44780GotoXY(uint8_t x, uint8_t y) {
  _HD44780Ready();
  _HD44780WriteData(_HD44780_BASE_Y[y] + x);
#ifdef HD44780_SCROLL
  _HD44780_x = x;
  _HD44780_y = y;
#endif
}

void HD44780PutChar(char c) {
#ifdef HD44780_SCROLL
  if (c == HD44780_NEWLINE) {
    goto NewLine;
  }
#endif
  _HD44780Ready();
  HD44780_RS_Set;
  _HD44780WriteData(c);
#ifdef HD44780_SCROLL
  _HD44780_x++;
  if (_HD44780_x >= HD44780_COLUMNS) {
  NewLine:
    _HD44780_y++;
    if (_HD44780_y >= HD44780_LINES) {
      uint8_t i, j;
      for (j = 0; j < (HD44780_LINES - 1); j++) {
        for (i = 0; i < HD44780_COLUMNS; i++) {
          _HD44780WriteByte(_HD44780_BASE_Y[j] + i,
                            _HD44780ReadByte(_HD44780_BASE_Y[j + 1] + i));
        }
      }
      HD44780ClearLine(HD44780_LINES - 1);
      HD44780GotoXY(0, HD44780_LINES - 1);
    } else {
      HD44780GotoXY(0, _HD44780_y);
    }
  }
#endif
}

#ifdef HD44780_SCROLL
void HD44780NewLine(void) { HD44780PutChar(HD44780_NEWLINE); }
#endif

void HD44780ClearLine(uint8_t line) {
  for (uint8_t i = 0; i < HD44780_COLUMNS; i++) {
    _HD44780WriteByte(_HD44780_BASE_Y[line] + i, ' ');
  }
}

// write the string str located in SRAM to the LCD
void HD44780Puts(char *str) {
  while (*str) {
    HD44780PutChar(*str);
    str++;
  };
}

void HD44780Putsf(const char *buf) {
  char c;
  for (;;) {
    c = (char)pgm_read_byte(buf);
    if (c == 0) {
      break;
    }
    HD44780PutChar(c);
    buf++;
  };
}

void HD44780DefineChar(const char *pc, char charCode) {
  uint8_t i, a;
  a = (charCode << 3) | 0x40;
  for (i = 0; i < 8; i++) {
    _HD44780WriteByte(a++, pgm_read_byte(pc));
    pc++;
  };
}
