#ifndef _HD44780_H_
#define _HD44780_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <avr/pgmspace.h>
#include <stdint.h>

#if defined(AVRCD324) || defined(AVRCD644) || defined(AVRCD1284)
#define HD44780_EN_BIT PA2
#define HD44780_EN_DDR DDRA
#define HD44780_EN_PORT PORTA

#define HD44780_RS_BIT PA0
#define HD44780_RS_DDR DDRA
#define HD44780_RS_PORT PORTA

#define HD44780_RW_BIT PA1
#define HD44780_RW_DDR DDRA
#define HD44780_RW_PORT PORTA

#define HD44780_DATA_DDR DDRA
#define HD44780_DATA_PORT PORTA
#define HD44780_DATA_PIN PINA
#define HD44780_DATA_MASK 0xF0
#define HD44780_DATA_BUSY 0x80

#define HD44780_COLUMNS 16
#define HD44780_LINES 2

#define HD44780BacklightingOn() {DDRA |= (1 << PA3); PORTA |= (1<<PA3);}
#define HD44780BacklightingOff() {PORTA &= ~(1<<PA3);}

//#define HD44780_SCROLL

#endif

#define HD44780_NEWLINE 10

void HD44780Init(void);
void HD44780Cursor(uint8_t c);
void HD44780Clear(void);
void HD44780GotoXY(uint8_t x, uint8_t y);
void HD44780PutChar(char c);
#ifdef HD44780_SCROLL
void HD44780NewLine(void);
#endif
void HD44780ClearLine(uint8_t line);
void HD44780Puts(char *str);
void HD44780Putsf(const char *buf);
void HD44780DefineChar(const char *pc, char charCode);

#ifdef __cplusplus
}
#endif

#endif //_HD44780_H_
