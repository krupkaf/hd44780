#include <HD44780.h>
const PROGMEM char charSet1[8] =
{0b00010101,
 0b00001010,
 0b00010101,
 0b00001010,
 0b00010101,
 0b00001010,
 0b00010101,
 0b00001010};
const PROGMEM char charSet2[8] =
{0b00001010,
 0b00010101,
 0b00001010,
 0b00010101,
 0b00001010,
 0b00010101,
 0b00001010,
 0b00010101};

void setup() {
        HD44780Init();
        HD44780DefineChar(charSet1, 1);
        HD44780DefineChar(charSet2, 2);
        HD44780GotoXY(0,0);
        HD44780Cursor(1);
}

uint8_t i = 0, j =0;
char str[] = " Hello";

void loop() {
        HD44780PutChar('0'+i);
        i++;
        j++;
        if (i>=10) {
                i=0;
        }
        if (j>=40) {
                j=0;
                HD44780NewLine();
        }
        if (j == 17) {
                HD44780Puts(str);
                HD44780Putsf(PSTR(" world! "));
        }
        if (j == 20) {
                HD44780PutChar(1);
                HD44780PutChar(2);
        }
        delay(1000);
}
